function findall(stre,pattern,modifiers="") {
	if(typeof(stre)!="string" || typeof(pattern)!="string") {alert("OOPS...I only take Strings as parameters....First one is the string to be matched and second one is the regex (string format) to be matched....");}
	else if(/[gG]/.test(modifiers)) {
		alert("This function always have to do global match...If you just want the first match try pattern.exec(str) instead..!!!")
	}
	else{
		var mainarr=[];
		var subarr=[];
		var modi="g"+modifiers;
		try {
			var patt = new RegExp(pattern, modi);
			var patt2 = new RegExp(pattern,modifiers);
			var allmatch=stre.match(patt);
			for (var i=0;i<allmatch.length;i++) {
				var everymatch=patt2.exec(allmatch[i]);
				if(everymatch!=null) {
					for (var j=1;j<everymatch.length;j++) {
						subarr.push(everymatch[j]);	
					}
					mainarr.push(subarr);
					subarr=[];
				}
			}
			return mainarr;
		}
		catch(err) {
			if (allmatch==null) {}
			
		}
		
		}	
}